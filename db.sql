DROP DATABASE IF EXISTS javajdbc;

CREATE DATABASE javajdbc DEFAULT CHARACTER SET = 'utf8mb4';

USE javajdbc;

CREATE TABLE breed(
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  label VARCHAR(64) UNIQUE
);

CREATE TABLE breeder(
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(64),
  address VARCHAR(64),
  zipcode VARCHAR(64),
  city VARCHAR(64)
);

CREATE TABLE dog(
  id INTEGER AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(64),
  age INTEGER,
  breed_id INTEGER,
  breeder_id INTEGER,
  CONSTRAINT FK_dog_breed FOREIGN KEY (breed_id) REFERENCES breed(id),
  CONSTRAINT FK_dog_breeder FOREIGN KEY (breeder_id) REFERENCES breeder(id)
);


-- contraintes : avoir au moins 2 Breed sans Dog, et 1 éleveur sans Dog, et un Dog sans éleveur
INSERT INTO breed(label) VALUES
('basset bleu de Gascogne'),
('berger'),
('bouledogue'),
('boxer'),
('colley'),
('dalmatien'),
('labrador'),
('shiba inu'),
('teckel'),
('Yorkshire terrier');

INSERT INTO breeder(name, address, zipcode, city) VALUES
('Golatière du Trépont', '19 Avenue Colonnes', '69671', 'Bron'),
('Combe Delacquis', '1707 Chem. de Rosarge', '01700', 'Les Echets'),
('Élevage du Jardin d''Arcos', '475 Chem. de la Rivière', '69380', 'Lozanne'),
('Elevage des Hauts de Valzan', 'Valzan', '42360', 'Cottance'),
('Red Dog', '25 Rue du Chariot d''Or', '69004', 'LYON'),
('Des Flocons Sibériens - AnimO’STAR 38', '124 Chem. des Guimonières', '38890', 'Saint-Chef'),
('Elevage de Cane Corso "Du Sang d''Orthos"', '3725 Rte de Cuiseaux', '71500', 'Bruailles'),
('Elevage Le Vert Logis', 'Le Golet', '42140', 'Chazelles-sur-Lyon'),
('Le Bois d''Eden', 'Joannet', '69690', 'Saint-Julien-sur-Bibost'),
('Chenil de la Ferme de Biesse', 'Rte de Biesse', '01480', 'Frans');


INSERT INTO dog(name, age, breed_id, breeder_id) VALUES
  ('Asko', 9, 3, 8),
  ('Atos', 15, 4, 3),
  ('Argos', 22, 2, 4),
  ('Pongo', 5, 6, 2),
  ('Perdita', 4, 6, 9),
  ('Top', 3, 1, 5),
  ('Gustav', 12, 7, 9);

-- Dog sans Breeder
INSERT INTO dog(name, age, breed_id) VALUES
('Lassie', 7, 5);

-- TODO:
-- Autojointures
-- Modifier la table Dog, pour ajouter 2 champs : id_father et id_mother.
-- Ces deux champs doivent permettre de retrouver les parents chiens du chien en cours.
-- Créez les clés étrangères correspondantes.
-- Le chien n°3 doit avoir les chiens 1 et 2 comme parents. Écrivez la requête correspondante
-- Écrivez la requête qui permet d'afficher le nom du chien n°3 et le nom de ses parents
