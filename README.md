# Java JDBC

Un projet avec un exemple de repository JDBC tel qu'on pourra en avoir pour n'importe quelle entité

## How To use
Si on clone le projet et qu'on veut juste le faire marcher (donc en sautant l'exercice "initialisation")
1. Cloner le projet
2. Créer une base de données (moi je l'ai appelé `p18_javajdbc`)
3. Importer le fichier [db.sql](db.sql) sur la base de données en question
4. Modifier les informations de connexion dans le fichier [DbUtil](src/main/java/co/simplon/promo18/repository/DbUtil.java) si nécessaire


## Exercices
### Initialisation
1. Créer un projet java maven et modifier le pom.xml pour y ajouter le mysql-connector-java et changer la version de java pour la 17 (projet co.simplon.promo18.javajdbc)
2. A la racine du projet, créer un fichier db.sql qui contiendra le create table pour une table Dog (qui aura donc un name en varchar, une breed en varchar, un age en int et un id donc), et rajouter en dessous quelques insert into
3. Créer la base de données via l'extension et faire un import du db.sql dessus
4. Dans le java, créer une entité entity.Dog en lui mettant ses propriétés/accesseurs/constructeurs
5. Créer ensuite une classe repository.DogRepository avec une méthode findAll dedans qui fera ira chercher tous les chiens sur la bdd (pour ça, inspirez vous de ce qu'on a fait mercredi dernier [ici même](https://gitlab.com/simplonlyon/promo18/first-sql/-/blob/main/src/main/java/co/simplon/promo18/repository/PersonRepository.java)

### Faire le findById
1. Dans le DogRepository, rajouter une méthode findById(int id) et qui va renvoyer un Dog
2. Cette méthode va commencer comme toutes les autres par une connexion à la base de données et un PreparedStatement
3. La requête qu'on veut exécuter ici est un SELECT en récupérant un dog spécifique par son id (donc déjà voir qu'est-ce qu'on ferait comme requête en SQL pour récupérer le chien avec l'id 1 par exemple), où l'id sera paramétrable comme on a fait dans la méthode save avec les ? et les stmt.set....
4. On exécute la query et on récupère le ResultSet, et vu que normalement on ne devrait pas récupérer plus qu'un dog avec un findById, on est pas obligé de faire de boucle pour le rs.next()
5. On crée une instance de Dog comme on l'a fait dans le findAll() et on la return directement, sans faire de List 

### Le delete
1. On rajoute une méthode deleteById(int id) qui ne va rien renvoyer et qui va commencer comme les autres (connexion/preparedstatement)
2. Cette fois la requête va être un DELETE en se basant sur l'id, donc ça va être exactement pareil que pour la méthode findById au niveau des paramètres
3. On exécute l'update, et voilà

### Le update
1. On rajoute une méthode update(Dog dog) qui ne va rien renvoyer et va aussi commencer comme toutes les autres
2. La requête sera un UPDATE où on modifiera toutes les valeurs du dog (son name, sa breed, son age) en se basant sur son id. On assigne les paramètre au statement (tout ça ressemblera beaucoup à la méthode save)
3. On exécute l'update, et voilà


## Les jointures, filtres, tris
### Modification de la structure de la base 
  - créer la table Breed, et centraliser la gestion des races
  - créer une table pour gérer les éleveurs (id, nom, adresse, cp, ville)
  - créer les clés étrangères qui vont bien
  - avoir le script db.sql qui crée la base, les tables, et fais quelques insert
  - contraintes : avoir au moins 2 Breed sans Dog, et 1 éleveur sans Dog, et un Dog sans éleveur

### La jointure avec WHERE et les alias
Écrire la requête qui permet d'afficher le nom du chien et le libellé de sa race. Utilisez les alias de tables et la clause WHERE

### Syntaxe JOIN
Modifier la requête précédente pour ne plus faire de jointure dans la clausse WHERE, mais en utilisant la syntaxe INNER JOIN

### Tri
  - Modifier la requête précédente pour afficher la liste des Dog par ordre alphabétique
  - Modifier la requête précédente pour afficher d'abord le label de la race. Trier le ResultSet par label de la race, puis par nom du chien, par ordre alphabétique.

### Filtres
 - Afficher le nom de tous les chiens de la race "Labrador"
 - Afficher le nom de l'éleveur n°2, et le nom de chacun des chiens qu'il a élevés
 - Afficher tous les éleveurs qui n'ont pas encore élevé de chiens (breeder.id n'est pas présent dans dog)
 - Pour chaque éleveur, afficher son nom, et la liste des **races de chiens** qu'il a élevés. Si un éleveur n'a pas encore élevé de chien, son nom doit apparaître quand même.

### Autojointures
  - Modifier la table Dog, pour ajouter 2 champs : id_father et id_mother. Ces deux champs doivent permettre de retrouver les parents chiens du chien en cours. Créez les clés étrangères correspondantes.
  - Le chien n°3 doit avoir les chiens 1 et 2 comme parents. Écrivez la requête correspondante
  - Écrivez la requête qui permet d'afficher le nom du chien n°3 et le nom de ses parents

## Fonctions de base

### Fonction COUNT
  - Afficher pour chaque race, le nombre de chiens de cette race
  - Afficher pour chaque éleveur, le nombre de races de chiens qu'il élève

### Fonctions MIN, MAX, AVG
  - Afficher l'âge du plus jeune chien
  - Afficher l'âge du plus vieux chien
  - Afficher l'âge moyen des chiens
  - Afficher le nom du plus jeune chien

### Fonction CONCAT
  - Afficher, dans une seule colonne de résultat, le nom du chien et sa race. Exemple : 

  > Bernard (cocker)

### Fonction COALESCE
