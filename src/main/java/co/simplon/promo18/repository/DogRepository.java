package co.simplon.promo18.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import co.simplon.promo18.entity.Dog;

public class DogRepository {
  public List<Dog> findAll() {
    List<Dog> list = new ArrayList<Dog>();
    
    try {
      Connection connection = DbUtil.connect();

      PreparedStatement select = connection.prepareStatement("SELECT * FROM dog");
      ResultSet rs = select.executeQuery();

      // table Dog qui aura un id, un name en varchar, une breed en varchar, un age en int
      while (rs.next()) {
        Dog dog = new Dog(
          rs.getInt("id"),
          rs.getString("name"),
          rs.getString("breed"),
          rs.getInt("age")
        );
        list.add(dog);
      }

      connection.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return list;

  }

// Faire le findById
// Dans le DogRepository, rajouter une méthode findById(int id) et qui va renvoyer un Dog
// Cette méthode va commencer comme toutes les autres par une connexion à la base de données et un PreparedStatement
// La requête qu'on veut exécuter ici est un SELECT en récupérant un dog spécifique par son id 
// (donc déjà voir qu'est-ce qu'on ferait comme requête en SQL pour récupérer le chien avec l'id 1 par exemple), 
// où l'id sera paramétrable comme on a fait dans la méthode save avec les ? et les stmt.set....
// On exécute la query et on récupère le ResultSet, et vu que normalement on ne devrait pas récupérer 
// plus d'un dog avec un findById, on est pas obligé de faire de boucle pour le rs.next() (mais if au cas ou, style assert)
// On crée une instance de Dog comme on l'a fait dans le findAll() et on la return directement, sans faire de List

  public Dog findbyID(int id) {

    try (Connection connection = DbUtil.connect()) {

      PreparedStatement select = connection.prepareStatement("SELECT * FROM dog WHERE id = ?");
      select.setInt(1, id);

      ResultSet rs = select.executeQuery();

      if (rs.next()) {
        Dog dog = new Dog(
          rs.getInt("id"),
          rs.getString("name"),
          rs.getString("breed"),
          rs.getInt("age")
        );

        return dog;
      } 
    } catch (SQLException e) {
      e.printStackTrace();
    }
    
    // catch-all
    return null;
    
  }

  public void save(Dog dog) {
    try {
      Connection connection = DbUtil.connect();

      // INSERT INTO dog(name, breed, age) VALUES ("Asko", "bouledogue", 9), 
      PreparedStatement stmt = connection.prepareStatement("INSERT INTO dog (name, breed, age) VALUES(?,?,?)",
          PreparedStatement.RETURN_GENERATED_KEYS);
      
      // JDBC est en base 1, les chiffres commencent à 1 et pas 0
      stmt.setString(1, dog.getName());
      stmt.setString(2, dog.getBreed());
      stmt.setInt(3, dog.getAge());
      
      stmt.executeUpdate();

      // recup de l'id

      ResultSet rs = stmt.getGeneratedKeys();
      if (rs.next()) {
        dog.setId(rs.getInt(1));
        
      }

      connection.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
  // Le delete
  // On rajoute une méthode deleteById(int id) qui ne va rien renvoyer et qui va commencer comme les autres (connexion/preparedstatement)
  // Cette fois la requête va être un DELETE en se basant sur l'id, 
  // donc ça va être exactement pareil que pour la méthode findById au niveau des paramètres
  // On exécute l'update, et voilà
  public void deleteById(int id) {
    try (Connection connection = DbUtil.connect()) {
      
      PreparedStatement stmt = connection.prepareStatement("DELETE FROM dog WHERE id = ?;");
      
      stmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    }

  }
  // Le update
  // On rajoute une méthode update(Dog dog) qui ne va rien renvoyer et va aussi commencer comme toutes les autres
  // La requête sera un UPDATE où on modifiera toutes les valeurs du dog (son name, sa breed, son age) en se basant sur son id. 
  // On assigne les paramètres au statement (tout ça ressemblera beaucoup à la méthode save)
  // On exécute l'update, et voilà
  public void update(Dog dog) {
    try (Connection connection = DbUtil.connect()) {
      //TODO

      // INSERT INTO dog(name, breed, age) VALUES ("Asko", "bouledogue", 9), 
      PreparedStatement stmt = connection.prepareStatement("INSERT INTO dog (name, breed, age) VALUES(?,?,?)",
          PreparedStatement.RETURN_GENERATED_KEYS);
      
      // JDBC est en base 1, les chiffres commencent à 1 et pas 0
      stmt.setString(1, dog.getName());
      stmt.setString(2, dog.getBreed());
      stmt.setInt(3, dog.getAge());
      
      stmt.executeUpdate();
      
    } catch (SQLException e) {
      e.printStackTrace();
    }

  }




}
