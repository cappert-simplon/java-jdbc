package co.simplon.promo18.entity;

// créer une table pour gérer les éleveurs (id, nom, adresse, cp, ville)
public class Breeder {
  private Integer id;
  private String name;
  private String address;
  private String zipcode;
  private String city;

  public Breeder(Integer id, String name, String address, String zipcode, String city) {
    this.id = id;
    this.name = name;
    this.address = address;
    this.zipcode = zipcode;
    this.city = city;
  }

  public Breeder(String name, String address, String zipcode, String city) {
    this.name = name;
    this.address = address;
    this.zipcode = zipcode;
    this.city = city;
  }

  public Breeder() {
    
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getZipcode() {
    return zipcode;
  }

  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

}
