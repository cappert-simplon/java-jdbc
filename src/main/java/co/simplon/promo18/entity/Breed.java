package co.simplon.promo18.entity;

public class Breed {
  private Integer id;
  private String label;

  public Breed(Integer id, String label) {
    this.id = id;
    this.label = label;
  }

  public Breed(String label) {
    this.label = label;
  }

  public Breed() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

}


