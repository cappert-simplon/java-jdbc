package co.simplon.promo18.entity;

public class Dog {

  // préferer Integer à int car Integer peut être null
  private Integer id;
  private String name;
  private String breed;
  private Integer age;

  public Dog(int id, String name, String breed, int age) {
    this.id = id;
    this.name = name;
    this.breed = breed;
    this.age = age;
  }

  public Dog(String name, String breed, int age) {
    this.name = name;
    this.breed = breed;
    this.age = age;
  }

  public Dog() {}

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBreed() {
    return breed;
  }

  public void setBreed(String breed) {
    this.breed = breed;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  
  
}



